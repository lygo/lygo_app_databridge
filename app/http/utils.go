package http

import (
	"bitbucket.org/lygo/lygo_app_databridge/app/commons"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_crypto"
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_commons/lygo_rnd"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"errors"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"net/url"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func WriteHtml(ctx *fiber.Ctx, html string) error {
	ctx.Response().Header.Set("Content-Type", "text/html")
	ctx.Response().SetBody([]byte(html))
	return nil
}

func WriteResponse(ctx *fiber.Ctx, data interface{}, err error) error {
	m := map[string]interface{}{}
	if nil != err {
		m["error"] = err.Error()
		if err == commons.HttpUnauthorizedError {
			ctx.Response().SetStatusCode(401)
			if nil == data {
				data = map[string]interface{}{"code": 401, "message": commons.HttpUnauthorizedError.Error()}
			}
		} else if err == commons.AccessTokenExpiredError {
			ctx.Response().SetStatusCode(401)
			if nil == data {
				data = map[string]interface{}{"code": 401, "message": commons.AccessTokenExpiredError.Error()}
			}
		} else if err == commons.RefreshTokenExpiredError {
			ctx.Response().SetStatusCode(401)
			if nil == data {
				data = map[string]interface{}{"code": 401, "message": commons.RefreshTokenExpiredError.Error()}
			}
		} else if err == commons.AccessTokenInvalidError {
			ctx.Response().SetStatusCode(401)
			if nil == data {
				data = map[string]interface{}{"code": 401, "message": commons.AccessTokenInvalidError.Error()}
			}
		} else {
			ctx.Response().SetStatusCode(500)
			if nil == data {
				data = map[string]interface{}{"code": 500, "message": err.Error()}
			}
		}
	}
	if nil != data {
		m["response"] = toJson(data)
	} else {
		m["response"] = "EMPTY_RESPONSE"
	}
	ctx.Response().Header.Set("Content-Type", "text/json")
	ctx.Response().SetBody([]byte(lygo_json.Stringify(m)))

	return nil
}

func GetAuthTokens(ctx *fiber.Ctx) []string {
	response := make([]string, 0)
	t := getApplicationToken(ctx)
	if len(t) > 0 {
		response = append(response, t)
	}
	t = getAuthenticationToken(ctx)
	if len(t) > 0 {
		response = append(response, t)
	}
	return response
}

func AuthenticateRequest(ctx *fiber.Ctx, authorization *commons.AuthorizationSettings, assert bool) bool {
	if nil != ctx {
		if nil != authorization {
			if len(authorization.Type) > 0 {
				// get check token
				requiredAuthToken, err := getAuthToken(authorization)
				if nil != err {
					_ = WriteResponse(ctx, nil, lygo_errors.Prefix(commons.HttpUnauthorizedError, err.Error()))
					return false
				}
				requiredAuthMode := authorization.Type
				if len(requiredAuthMode) > 0 && requiredAuthMode != "none" {
					// get access token
					token := getAuthenticationToken(ctx)
					if len(token) == 0 {
						token = getApplicationToken(ctx)
					}
					if len(token) == 0 && assert {
						_ = WriteResponse(ctx, nil, lygo_errors.Prefix(commons.HttpUnauthorizedError, "Missing Authorization Token:"))
						return false
					}
					if len(requiredAuthToken) > 0 {
						// direct check
						if token != requiredAuthToken {
							_ = WriteResponse(ctx, nil, commons.HttpUnauthorizedError)
							return false
						}
					} else {
						// access token validation
						_ = WriteResponse(ctx, nil, commons.AccessTokenExpiredError)
						return false
					}
				}
			}
		}
	}
	return true
}

func BodyMap(ctx *fiber.Ctx, flatData bool) map[string]interface{} {
	var m map[string]interface{}
	body := ctx.Body()
	_ = lygo_json.Read(body, &m)

	if data, b := m["data"].(map[string]interface{}); b && flatData {
		delete(m, "data")
		for k, v := range data {
			m[k] = v
		}
	}

	return m
}

func Params(ctx *fiber.Ctx, flatData bool, selectOnly ...string) map[string]interface{} {
	// get all body params
	response := BodyMap(ctx, flatData)
	if nil == response {
		response = make(map[string]interface{})
	}

	// try add form params
	if form, err := ctx.MultipartForm(); nil == err && nil != form && nil != form.Value {
		for k, v := range form.Value {
			if _, b := response[k]; !b {
				if len(v) == 1 {
					response[k] = v[0]
				} else {
					response[k] = v
				}
			}
		}
	}

	// route
	routeParams := ctx.Route().Params
	if len(routeParams) > 0 {
		for _, rp := range routeParams {
			response[rp] = ctx.Params(rp)
		}
	}

	// url query
	if path := ctx.OriginalURL(); len(path) > 0 {
		uri, err := url.Parse(path)
		if nil == err {
			query := uri.Query()
			if nil != query && len(query) > 0 {
				for k, v := range query {
					if len(v) == 1 {
						response[k] = v[0]
					} else {
						response[k] = v
					}
				}
			}
		}
	}

	// evaluate filter
	if len(selectOnly) > 0 {
		m := map[string]interface{}{}
		for _, name := range selectOnly {
			if v, b := response[name]; b {
				m[name] = v
			}
		}
		return m
	}

	return response
}

func AssertParams(ctx *fiber.Ctx, names []string) (map[string]interface{}, error) {
	params := Params(ctx, true)
	if len(names) > 0 {
		missing := make([]string, 0)
		// get missing parameters
		for _, name := range names {
			if value, b := params[name]; !b || len(lygo_conv.ToString(value)) == 0 {
				missing = append(missing, name)
			}
		}
		if len(missing) > 0 {
			return nil, errors.New(fmt.Sprintf("missing_params:%v", strings.Join(missing, ",")))
		}
	}
	return params, nil
}

func Upload(ctx *fiber.Ctx, root string, sizeLimit int64) ([]string, error) {
	// get form
	form, err := ctx.MultipartForm()
	if nil != err {
		return nil, err // not a form request
	}

	root = lygo_paths.Absolute(root) // absolute

	response := make([]string, 0)
	// loop on files
	for _, files := range form.File {
		// Loop through files:
		for _, file := range files {
			size := file.Size
			if sizeLimit > 0 && size > sizeLimit {
				continue
			}
			filename := file.Filename
			ext := lygo_paths.Extension(filename)
			name := lygo_paths.FileName(filename, false)
			dir := lygo_paths.Dir(filename)
			filename = lygo_paths.Concat(dir, name+"_"+lygo_crypto.MD5(lygo_rnd.Uuid())+ext)
			path := lygo_paths.DatePath(root, filename, 3, true)
			err = ctx.SaveFile(file, path)
			if nil != err {
				return response, err
			}
			response = append(response, strings.Replace(path, root, ".", 1)) // relative path
		}
	}
	return response, nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func toJson(data interface{}) interface{} {
	if nil != data {
		return lygo_json.Parse(lygo_conv.ToString(data))
	}
	return nil
}

func getAuthToken(auth *commons.AuthorizationSettings) (string, error) {
	value := auth.Value
	switch auth.Type {
	case "base", "basic":
		// BASE AUTH
		if strings.Index(value, ":") > -1 {
			return value, nil
		}
		data, err := lygo_crypto.DecodeBase64(value)
		if nil != err {
			return "", err
		}
		return string(data), nil
	case "bearer":
		// BEARER AUTH
		return value, nil
	}
	return value, nil
}

func getAuthenticationToken(ctx *fiber.Ctx) string {
	data := getAuthentication(ctx)
	accessToken := lygo_reflect.GetString(data, "token")
	if len(accessToken) == 0 {
		// look into params
		params := Params(ctx, true)
		accessToken = lygo_reflect.GetString(params, "access_token")
	}
	return accessToken
}

func getApplicationToken(ctx *fiber.Ctx) string {
	// look into params
	params := Params(ctx, true)
	token := lygo_reflect.GetString(params, "token")
	if len(token) == 0 {
		token = lygo_reflect.GetString(params, "app_token")
		if len(token) == 0 {
			token = lygo_reflect.GetString(params, "application_token")
			if len(token) == 0 {
				token = lygo_reflect.GetString(params, "auth_token")
				if len(token) == 0 {
					token = lygo_reflect.GetString(params, "app-token")
					if len(token) == 0 {
						token = lygo_reflect.GetString(params, "auth-token")
					}
				}
			}
		}
	}
	return token
}

func getAuthentication(ctx *fiber.Ctx) map[string]string {
	response := map[string]string{}

	v := ctx.Get("Authorization", "")
	tokens := lygo_strings.Split(v, " ")
	response["authorization"] = v

	if len(tokens) == 2 {
		mode := tokens[0]
		response["mode"] = mode
		switch mode {
		case "Bearer":
			response["token"] = tokens[1]
		case "Basic":
			response["username"] = ""
			response["password"] = ""
			data, _ := lygo_crypto.DecodeBase64(tokens[1])
			response["token"] = string(data)
			if len(data) > 0 {
				t := strings.Split(string(data), ":")
				if len(t) == 2 {
					response["username"] = t[0]
					response["password"] = t[1]
				}
			}
		}
	}

	return response
}
