package http

import (
	"bitbucket.org/lygo/lygo_app_databridge/app/commons"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
)

const (
	URL_SIGNIN                  = "/api/guardian/auth/sign-in"
	URL_SIGNUP                  = "/api/guardian/auth/sign-up"
	URL_VERIFY                  = "/api/guardian/auth/verify/:token"
	URL_FORGOT_PASSWORD         = "/api/guardian/auth/forgot-password"
	URL_CHANGE_PASSWORD         = "/api/guardian/auth/change-password/:token"
	URL_RESET                   = "/api/guardian/auth/reset-password"
	URL_REMOVE                  = "/api/guardian/auth/remove"
	URL_IMPORT                  = "/api/guardian/auth/import"
	URL_RESEND_ACTIVATION_EMAIL = "/api/guardian/auth/resend-activation-email"
	URL_VALIDATE_TOKEN          = "/api/guardian/auth/validate-token"
	URL_REFRESH_TOKEN           = "/api/guardian/auth/refresh-token"
	URL_DELEGATE_GRANT          = "/api/guardian/auth/delegate-grant"
	URL_DELEGATE_REVOKE         = "/api/guardian/auth/delegate-revoke"
)

type WebServerSettingsRoute struct {
	Method        string                         `json:"method"`
	Endpoint      string                         `json:"endpoint"`
	Authorization *commons.AuthorizationSettings `json:"authorization"`
}

type WebServerSettingsRouting struct {
	WebServerSettingsRoute
}

type WebServerSettingsRouteAuth struct {
	WebServerSettingsRoute
	ParamsRequired []string `json:"params-required"`
	Params         []string `json:"params"`
}
type WebServerSettingsAuthenticationRouting struct {
	AuthSignIn                *WebServerSettingsRouteAuth `json:"sign-in"` // login
	AuthSignUp                *WebServerSettingsRouteAuth `json:"sign-up"` // new user
	AuthVerify                *WebServerSettingsRouteAuth `json:"verify"`  // confirm account
	AuthForgotPassword        *WebServerSettingsRouteAuth `json:"forgot-password"`
	AuthChangePassword        *WebServerSettingsRouteAuth `json:"change-password"`
	AuthResetPassword         *WebServerSettingsRouteAuth `json:"reset-password"`
	AuthResendActivationEmail *WebServerSettingsRouteAuth `json:"resend-activation-email"`
	AuthRemove                *WebServerSettingsRouteAuth `json:"remove"`
	AuthImport                *WebServerSettingsRouteAuth `json:"import"`
	AuthValidateToken         *WebServerSettingsRouteAuth `json:"validate-token"`
	AuthRefreshToken          *WebServerSettingsRouteAuth `json:"refresh-token"`
	AuthGrantDelegation       *WebServerSettingsRouteAuth `json:"delegate-grant"`
	AuthRevokeDelegation      *WebServerSettingsRouteAuth `json:"delegate-revoke"`
}
type WebServerSettingsAuthenticationPostman struct {
	Payload    map[string]interface{} `json:"payload"`
	ConfigMail map[string]interface{} `json:"config-mail"`
	ConfigSms  map[string]interface{} `json:"config-sms"`
}

type WebServerSettingsAuthenticationSecurityAuthorization struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}

type WebServerSettingsAuthenticationSecurity struct {
	AppToken      string                                                `json:"app-token"`
	Authorization *WebServerSettingsAuthenticationSecurityAuthorization `json:"authorization"`
}

func (instance *WebServerSettingsAuthenticationSecurity) Map() map[string]interface{} {
	return lygo_conv.ToMap(instance)
}

type WebServerSettingsAuthentication struct {
	Enabled                         bool                                     `json:"enabled"`
	PasswordExpireInDays            int                                      `json:"password-expire-days"`
	AccountNotConfirmedExpireInDays int                                      `json:"account-not-confirmed-expire-days"`
	AppBaseUrl                      string                                   `json:"app-base-url"`
	ApiBaseUrl                      string                                   `json:"api-base-url"`
	Security                        *WebServerSettingsAuthenticationSecurity `json:"security"`
	DirTemplates                    string                                   `json:"dir-templates"`
	DbUsersCollection               string                                   `json:"db-users-collection"`
	Postman                         *WebServerSettingsAuthenticationPostman  `json:"postman"`
	Routing                         *WebServerSettingsAuthenticationRouting  `json:"routing"`
}

type WebServerSettings struct {
	Enabled        bool                             `json:"enabled"`
	Http           map[string]interface{}           `json:"http"`
	Routing        []*WebServerSettingsRouting      `json:"routing"`
	Authentication *WebServerSettingsAuthentication `json:"authentication"`
}

func (instance *WebServerSettings) Map() map[string]interface{} {
	return lygo_conv.ToMap(instance)
}

func NewWebServerSettingsAuthenticationRouting() *WebServerSettingsAuthenticationRouting {
	response := new(WebServerSettingsAuthenticationRouting)

	response.AuthSignIn = new(WebServerSettingsRouteAuth)
	response.AuthSignIn.Method = "POST"
	response.AuthSignIn.Endpoint = URL_SIGNIN
	response.AuthSignIn.ParamsRequired = []string{"email", "password"}
	response.AuthSignIn.Params = []string{}

	response.AuthSignUp = new(WebServerSettingsRouteAuth)
	response.AuthSignUp.Method = "POST"
	response.AuthSignUp.Endpoint = URL_SIGNUP
	response.AuthSignUp.ParamsRequired = []string{"email", "password", "firstname", "lastname", "privacy_policy_consent"}
	response.AuthSignUp.Params = []string{"email", "password", "firstname", "lastname", "privacy_policy_consent"}

	response.AuthRemove = new(WebServerSettingsRouteAuth)
	response.AuthRemove.Method = "POST"
	response.AuthRemove.Endpoint = URL_REMOVE
	response.AuthRemove.ParamsRequired = []string{"_key"}
	response.AuthRemove.Params = []string{}

	response.AuthVerify = new(WebServerSettingsRouteAuth)
	response.AuthVerify.Method = "GET"
	response.AuthVerify.Endpoint = URL_VERIFY
	response.AuthVerify.ParamsRequired = []string{"token"}
	response.AuthVerify.Params = []string{}

	response.AuthResendActivationEmail = new(WebServerSettingsRouteAuth)
	response.AuthResendActivationEmail.Method = "POST"
	response.AuthResendActivationEmail.Endpoint = URL_RESEND_ACTIVATION_EMAIL
	response.AuthResendActivationEmail.ParamsRequired = []string{"email"}
	response.AuthResendActivationEmail.Params = []string{"email", ".addresses", ".channels"}

	response.AuthForgotPassword = new(WebServerSettingsRouteAuth)
	response.AuthForgotPassword.Method = "POST"
	response.AuthForgotPassword.Endpoint = URL_FORGOT_PASSWORD
	response.AuthForgotPassword.ParamsRequired = []string{"email"}
	response.AuthForgotPassword.Params = []string{}

	response.AuthChangePassword = new(WebServerSettingsRouteAuth)
	response.AuthChangePassword.Method = "GET"
	response.AuthChangePassword.Endpoint = URL_CHANGE_PASSWORD
	response.AuthChangePassword.ParamsRequired = []string{"token"}
	response.AuthChangePassword.Params = []string{}

	response.AuthResetPassword = new(WebServerSettingsRouteAuth)
	response.AuthResetPassword.Method = "POST"
	response.AuthResetPassword.Endpoint = URL_RESET
	response.AuthResetPassword.ParamsRequired = []string{"auth_id", "password"}
	response.AuthResetPassword.Params = []string{"auth_id", "password", "email"}

	response.AuthImport = new(WebServerSettingsRouteAuth)
	response.AuthImport.Method = "POST"
	response.AuthImport.Endpoint = URL_IMPORT
	response.AuthImport.ParamsRequired = []string{"fldEmail"}
	response.AuthImport.Params = []string{}

	response.AuthValidateToken = new(WebServerSettingsRouteAuth)
	response.AuthValidateToken.Method = "POST"
	response.AuthValidateToken.Endpoint = URL_VALIDATE_TOKEN
	response.AuthValidateToken.ParamsRequired = []string{"token"}
	response.AuthValidateToken.Params = []string{}

	response.AuthRefreshToken = new(WebServerSettingsRouteAuth)
	response.AuthRefreshToken.Method = "POST"
	response.AuthRefreshToken.Endpoint = URL_REFRESH_TOKEN
	response.AuthRefreshToken.ParamsRequired = []string{"token"}
	response.AuthRefreshToken.Params = []string{}

	response.AuthGrantDelegation = new(WebServerSettingsRouteAuth)
	response.AuthGrantDelegation.Method = "POST"
	response.AuthGrantDelegation.Endpoint = URL_DELEGATE_GRANT
	response.AuthGrantDelegation.ParamsRequired = []string{"ownerAccessToken"}
	response.AuthGrantDelegation.Params = []string{}

	response.AuthRevokeDelegation = new(WebServerSettingsRouteAuth)
	response.AuthRevokeDelegation.Method = "POST"
	response.AuthRevokeDelegation.Endpoint = URL_DELEGATE_REVOKE
	response.AuthRevokeDelegation.ParamsRequired = []string{"delegationToken"}
	response.AuthRevokeDelegation.Params = []string{}

	return response
}
