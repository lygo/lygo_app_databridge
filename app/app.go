package app

import (
	"bitbucket.org/lygo/lygo_app_databridge/app/commons"
	"bitbucket.org/lygo/lygo_app_databridge/app/database"
	"bitbucket.org/lygo/lygo_app_databridge/app/webserver"
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_events"
	"path/filepath"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type App struct {
	root            string
	dirStart        string
	dirApp          string
	dirWork         string
	logger          *commons.Logger
	settings        *commons.AppSettings
	mode            string
	stopChan        chan bool
	events          *lygo_events.Emitter
	databaseManager *database.AppDatabase
	webserver       *webserver.AppWebserver
	pathVariables   map[string]string
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewApp(mode string, settings *commons.AppSettings) (instance *App, err error) {
	instance = new(App)

	instance.dirStart = lygo_paths.GetWorkspace(commons.WpDirStart).GetPath()
	instance.dirApp = lygo_paths.GetWorkspace(commons.WpDirApp).GetPath()
	instance.dirWork = lygo_paths.GetWorkspace(commons.WpDirWork).GetPath()
	instance.root = filepath.Dir(instance.dirWork)

	instance.pathVariables = make(map[string]string)
	instance.pathVariables[commons.PathDirStart] = instance.dirStart
	instance.pathVariables[commons.PathDirApp] = instance.dirApp
	instance.pathVariables[commons.PathDirWork] = instance.dirWork
	instance.pathVariables[commons.PathDirPrograms] = filepath.Join(instance.dirWork, "programs")
	instance.pathVariables[commons.PathDirModules] = filepath.Join(instance.dirWork, "modules")

	instance.logger = commons.NewLogger(mode)
	if nil == settings {
		instance.settings, err = commons.NewAppSettings(mode)
	} else {
		instance.settings = settings
	}
	instance.mode = mode
	instance.stopChan = make(chan bool, 1)
	instance.events = lygo_events.NewEmitter()

	if len(instance.settings.LogLevel) > 0 {
		instance.logger.SetLevel(instance.settings.LogLevel)
	}

	instance.databaseManager = database.NewAppDatabase(instance.logger, instance.settings.Database)
	instance.webserver = webserver.NewAppWebserver(mode, instance.dirWork, instance.logger,
		instance.settings.Authorization, instance.databaseManager)

	// handle internal events
	instance.events.On(commons.EventOnDoStop, instance.handleDoStopEvent)

	return instance, err
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *App) GetLogger() *commons.Logger {
	if nil != instance {
		return instance.logger
	}
	return nil
}

func (instance *App) Start() (err error) {
	if nil != instance {
		// start scheduler
		err = instance.start()

		return err
	}
	return commons.PanicSystemError
}

func (instance *App) Stop() (err error) {
	if nil != instance {
		// start scheduler
		err = instance.stop()
		return
	}
	return commons.PanicSystemError
}

func (instance *App) Wait() {
	// wait exit
	<-instance.stopChan
	// reset channel
	instance.stopChan = make(chan bool, 1)
}

func (instance *App) WaitTimeout(duration time.Duration) {
	go func() {
		time.Sleep(duration)
		_ = instance.Stop()
	}()
	instance.Wait()
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *App) start() error {
	if nil != instance {
		if nil != instance.settings {
			instance.webserver.Start()
			_ = instance.databaseManager.Start()

			return nil
		}
		return lygo_errors.Prefix(commons.InvalidConfigurationError, "Missing Settings: ")
	}
	return commons.PanicSystemError
}

func (instance *App) stop() error {
	if nil != instance {

		if nil != instance.stopChan {
			instance.webserver.Stop()
			instance.databaseManager.Stop()

			instance.stopChan <- true
			instance.stopChan = nil
		}

		return nil
	}
	return commons.PanicSystemError
}

func (instance *App) handleDoStopEvent(_ *lygo_events.Event) {
	if nil != instance {
		_ = instance.Stop()
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------
