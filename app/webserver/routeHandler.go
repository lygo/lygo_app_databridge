package webserver

import (
	"bitbucket.org/lygo/lygo_app_databridge/app/commons"
	"bitbucket.org/lygo/lygo_app_databridge/app/http"
	"github.com/gofiber/fiber/v2"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type ExecutorCallback func(ctx *fiber.Ctx, endpoint *commons.EndPoint) error

type RouteHandler struct {
	method        string
	endpoint      *commons.EndPoint
	authorization *commons.AuthorizationSettings
	callback      ExecutorCallback
}

func NewRouteHandler(config *http.WebServerSettingsRouting, callback ExecutorCallback) *RouteHandler {
	instance := new(RouteHandler)
	instance.method = strings.ToLower(config.Method)
	instance.endpoint = commons.ParseEndPoint( config.Endpoint )
	instance.authorization = config.Authorization
	instance.callback = callback

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *RouteHandler) Method() string {
	if nil != instance {
		return instance.method
	}
	return ""
}

func (instance *RouteHandler) Endpoint() *commons.EndPoint {
	if nil != instance {
		return instance.endpoint
	}
	return nil
}

func (instance *RouteHandler) Handle(ctx *fiber.Ctx) error {
	if nil != instance {
		err := instance.run(ctx)
		if instance.isMiddleware() {
			return ctx.Next()
		}
		return err
	}

	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *RouteHandler) isReady() bool {
	if nil != instance && nil != instance.callback {
		return true
	}
	return false
}

func (instance *RouteHandler) isMiddleware() bool {
	if nil != instance {
		return instance.method == "middleware"
	}
	return false
}

func (instance *RouteHandler) run(ctx *fiber.Ctx) error {
	if nil != instance && instance.isReady() && nil != instance.callback {
		if http.AuthenticateRequest(ctx, instance.authorization, false) {
			return instance.callback(ctx, instance.endpoint)
		} else {
			return commons.HttpUnauthorizedError
		}
	}
	return commons.EndpointNotReadyError
}
