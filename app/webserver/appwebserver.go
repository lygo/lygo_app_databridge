package webserver

import (
	"bitbucket.org/lygo/lygo_app_databridge/app/commons"
	"bitbucket.org/lygo/lygo_app_databridge/app/database"
	"bitbucket.org/lygo/lygo_app_databridge/app/http"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"github.com/gofiber/fiber/v2"
)

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type AppWebserver struct {
	mode         string
	dirWork      string //  workspace
	logger       *commons.Logger
	authSettings *commons.AuthorizationSettings
	database     *database.AppDatabase
	webserver    *http.Webserver
}

func NewAppWebserver(mode string, dirWork string, l *commons.Logger, authSettings *commons.AuthorizationSettings, database *database.AppDatabase) *AppWebserver {
	instance := new(AppWebserver)
	instance.mode = mode
	instance.dirWork = dirWork
	instance.logger = l
	instance.database = database
	instance.authSettings = authSettings
	instance.webserver = http.NewWebserver("webserver", mode, dirWork)

	_ = instance.initHandlers()

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppWebserver) Start() {
	if nil != instance && nil != instance.webserver {
		if instance.webserver.Start() {
			// SUCCESS
		}
	}
}

func (instance *AppWebserver) Stop() {
	if nil != instance && nil != instance.webserver {
		instance.webserver.Stop()
	}
}

func (instance *AppWebserver) LocalUrl() string {
	if nil != instance && nil != instance.webserver {
		return instance.webserver.LocalUrl()
	}
	return ""
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppWebserver) initHandlers() error {
	if nil != instance && nil != instance.webserver && instance.webserver.IsEnabled() && nil != instance.webserver.Settings() {
		handlers := make([]http.IRouteHandler, 0)
		routing := instance.webserver.Settings().Routing
		for _, route := range routing {
			if nil == route.Authorization {
				route.Authorization = instance.authSettings // replace with defaults
			}
			handler := NewRouteHandler(route, instance.onRoute)
			handlers = append(handlers, handler)
		}
		return instance.webserver.Initialize(handlers)
	}
	return nil
}

func (instance *AppWebserver) onRoute(ctx *fiber.Ctx, endpoint *commons.EndPoint) error {
	// handle
	switch endpoint.Version {
	case "v1":
		switch endpoint.Name {
		case "execute":
			body := http.BodyMap(ctx, true)
			command := lygo_reflect.GetString(body, "command")
			if len(command) > 0 {
				params := lygo_conv.ToMap(lygo_reflect.Get(body, "params"))
				response, err := instance.database.Execute(command, params)
				return http.WriteResponse(ctx, response, err)
			} else {
				return commons.MissingParamError
			}
		default:
			// unsupported method
			return commons.HttpUnsupportedApiMethodError
		}
	default:
		// unsupported version
		return commons.HttpUnsupportedApiVersionError
	}
	return nil
}
