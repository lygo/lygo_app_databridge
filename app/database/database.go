package database

import (
	"bitbucket.org/lygo/lygo_app_databridge/app/commons"
	"bitbucket.org/lygo/lygo_ext_dbal/dbal_drivers"
	_ "github.com/alexbrainman/odbc"
)

// ---------------------------------------------------------------------------------------------------------------------
//	AppDatabase
// ---------------------------------------------------------------------------------------------------------------------

type AppDatabase struct {
	logger   *commons.Logger
	settings *commons.DatabaseSettings

	__db dbal_drivers.IDatabase
}

func NewAppDatabase(logger *commons.Logger, settings *commons.DatabaseSettings) *AppDatabase {
	instance := new(AppDatabase)
	instance.logger = logger
	instance.settings = settings

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppDatabase) Start() (err error) {
	_, err = instance.db()
	return
}

func (instance *AppDatabase) Stop() {
	if db, err := instance.db(); nil == err {
		_ = db.Close()
	}
	return
}

func (instance *AppDatabase) Execute(command string, params map[string]interface{}) (interface{}, error) {
	if db, err := instance.db(); nil == err {
		return db.ExecNative(command, params)
	} else {
		return nil, err
	}
	return nil, nil
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppDatabase) db() (dbal_drivers.IDatabase, error) {
	if nil != instance && nil != instance.settings {
		if nil == instance.__db {
			db, err := dbal_drivers.NewDatabase(instance.settings.Driver, instance.settings.Dsn)
			if nil != err {
				return nil, err
			}
			instance.__db = db
		}
		return instance.__db, nil
	}
	return nil, commons.PanicSystemError
}
