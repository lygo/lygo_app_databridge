package commons

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"github.com/cbroglie/mustache"
)

// ---------------------------------------------------------------------------------------------------------------------
//	ActionVariables
// ---------------------------------------------------------------------------------------------------------------------

type DatabaseSettings struct {
	Driver string `json:"driver"`
	Dsn    string `json:"dsn"`
}

type AuthorizationSettings struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}

func AuthorizationSettingsFromMap(m map[string]interface{}) (*AuthorizationSettings, error) {
	var response *AuthorizationSettings
	err := lygo_json.Read(lygo_json.Stringify(m), &response)
	if nil != err {
		return nil, err
	}
	return response, nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	AppSettings
// ---------------------------------------------------------------------------------------------------------------------

type AppSettings struct {
	Silent        bool                   `json:"silent"`
	LogLevel      string                 `json:"log-level"`
	Database      *DatabaseSettings      `json:"database"`
	Authorization *AuthorizationSettings `json:"authorization"`
}

func NewAppSettings(mode string) (*AppSettings, error) {
	settings := new(AppSettings)
	err := ensureSettingsFilesExists(mode)
	if nil != err {
		return settings, err
	}
	path := lygo_paths.WorkspacePath("settings." + mode + ".json")
	text, err := lygo_io.ReadTextFromFile(path)
	if nil != err {
		return settings, err
	}
	context := make(map[string]interface{})
	context["workspace"] = lygo_paths.GetWorkspacePath()
	text, err = mustache.Render(text, context)
	if nil != err {
		return settings, err
	}
	err = lygo_json.Read(text, &settings)
	return settings, err
}

func NewGuardianEmptySettings() *AppSettings {
	instance := new(AppSettings)
	instance.LogLevel = "debug"
	instance.Silent = false

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *AppSettings) Parse(text string) error {
	return lygo_json.Read(text, &instance)
}

func (instance *AppSettings) String() string {
	return lygo_json.Stringify(instance)
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func ensureSettingsFilesExists(mode string) error {
	err := writeFile(lygo_paths.WorkspacePath("settings."+mode+".json"), TplFileSettings, map[string]interface{}{"Mode": mode})
	if nil != err {
		return err
	}

	err = writeFile(lygo_paths.WorkspacePath("webserver."+mode+".json"), TplFileWebserver, map[string]interface{}{"Mode": mode})
	if nil != err {
		return err
	}

	return nil
}

func writeFile(filename string, tpl string, data interface{}) error {
	if b, _ := lygo_paths.Exists(filename); !b {
		text, err := MergeTpl(tpl, data)
		if nil != err {
			return err
		}
		_, err = lygo_io.WriteTextToFile(text, filename)
		if nil != err {
			return err
		}
	}
	return nil
}
