package commons

import "errors"

const (
	Version = "1.1.0"
	Name    = "Data Bridge"

	ModeProduction = "production"
	ModeDebug      = "debug"

	EventOnUpgrade = "on_upgrade"
	EventOnStart   = "on_start"
	EventOnQuit    = "on_quit"
	EventOnError   = "on_error"
	EventOnDoStop  = "on_do_stop"

	// workspaces
	WpDirStart = "start"
	WpDirApp   = "app"
	WpDirWork  = "*"

	// path variables
	PathDirStart    = "$dir_start"
	PathDirApp      = "$dir_app"
	PathDirWork     = "$dir_work"
	PathDirPrograms = "$dir_programs"
	PathDirModules  = "$dir_modules"
	PathDirProgram  = "$dir_program"

	// js variables
	VarDirStart    = "_dir_start" // __dir_start
	VarDirApp      = "_dir_app"
	VarDirWork     = "_dir_work"
	VarDirPrograms = "_dir_programs"
	VarDirModules  = "_dir_modules"
	VarFilename    = "_filename"
	VarDirname     = "_dirname"
	VarUID         = "_uid"
)

// ---------------------------------------------------------------------------------------------------------------------
//		e r r o r s
// ---------------------------------------------------------------------------------------------------------------------

var (
	PanicSystemError          = errors.New("panic_system_error")
	InvalidConfigurationError = errors.New("invalid_configuration_error")
	MissingFileNameError = errors.New("missing_file_name")

	EndpointNotReadyError = errors.New("endpoint_not_ready")

	MissingParamError = errors.New("missing_param")

	HttpUnauthorizedError = errors.New("unauthorized") // 401
	AccessTokenExpiredError  = errors.New("access_token_expired")  // 401
	RefreshTokenExpiredError = errors.New("refresh_token_expired") // 401
	AccessTokenInvalidError = errors.New("access_token_invalid") // 401
	HttpUnsupportedApiVersionError = errors.New("unsupported_api")
	HttpUnsupportedApiMethodError = errors.New("unsupported_method")
)
