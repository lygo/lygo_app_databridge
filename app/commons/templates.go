package commons

import (
	"bytes"
	_ "embed"
	"strings"
	"text/template"
)

//go:embed settings.json
var TplFileSettings string

//go:embed webserver.json
var TplFileWebserver string


func MergeTpl(text string, data interface{}) (string, error) {
	if nil==data{
		data = struct {}{}
	}
	buff := bytes.NewBufferString("")
	t := template.Must(template.New("template").Parse(strings.Trim(text, "\n")))
	err := t.Execute(buff, data)
	if nil != err {
		return "", err
	}
	return buff.String(), err
}