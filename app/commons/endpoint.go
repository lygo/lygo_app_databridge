package commons

import (
	"fmt"
	"strings"
)

// EndPoint "/api/v1/commands/execute"
type EndPoint struct {
	Mode    string // api
	Version string // v1
	Group   string // commands
	Name    string // execute
}

func ParseEndPoint(url string) *EndPoint {
	instance := new(EndPoint)
	tokens := strings.Split(url, "/")
	count := 0
	for _, token := range tokens {
		if len(token) > 0 {
			count++
			switch count {
			case 1:
				instance.Mode = token
			case 2:
				instance.Version = token
			case 3:
				instance.Group = token
			case 4:
				instance.Name = token
			}
		}
	}
	return instance
}

func (instance *EndPoint) String() string {
	return fmt.Sprintf("/%v/%v/%v/%v", instance.Mode, instance.Version, instance.Group, instance.Name)
}
