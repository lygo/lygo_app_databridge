# Data Bridge #
![](./icon.png)

Data Bridge allow to send commands to any supported database and get back e JSON response wrapping data.

## Configuration ##

### DATABASE ###

File: settings.json

``` 
{
  "silent": true,
  "log-level": "debug",
  "database": {
    "driver": "odbc",
    "dsn": "server=79.11.253.77,1433;database=DB-NAME;uid=USER;pwd=PASSWORD;TDS_Version=8.0;"
  },
  "authorization": {
        "type": "none",
        "value": ""
  }
}
``` 

### WEB SERVER ###

File: webserver.json

``` 
{
  "enabled": true,
  "http": {
    "root": "./webserver",
    "server": {
      "enable_request_id": true,
      "prefork": false
    },
    "hosts": [
      {
        "addr": ":8000",
        "tls": false,
        "websocket": {
          "enabled": true
        }
      },
      {
        "addr": ":443",
        "tls": true,
        "ssl_cert": "./cert/ssl-cert.pem",
        "ssl_key": "./cert/ssl-cert.key",
        "websocket": {
          "enabled": true
        }
      }
    ],
    "static": [
      {
        "enabled": true,
        "prefix": "/",
        "root": "./www",
        "index": "",
        "compress": true
      }
    ],
    "compression": {
      "enabled": false,
      "level": 0
    },
    "limiter": {
      "enabled": false,
      "timeout": 30,
      "max": 10
    },
    "CORS": {
      "enabled": true
    }
  },
  "routing": [
    {
      "method": "POST",
      "endpoint": "/api/v1/commands/execute",
      "authorization": {
        "type": "none",
        "value": ""
      }
    }
  ]
}
``` 

## Usage ##

URL:
``` 
/api/v1/commands/execute
```
REQUEST BODY (Method POST)
```
{
    "command":"SELECT COUNT(DISTINCT codice) FROM dbo.banche WHERE id=@id",
    "params":{
        "id":1    
    }
}
```

## ODBC installation in Linux ##

To install ODBC drivers you need do this tasks:
 - Install unixODBC 
 - Install ODBC Drivers for MSSQL Server

### Install unixODBC  ###

Follow this tutorial to download and build unixODBC

https://www.osradar.com/how-to-install-odbc-on-ubuntu-20-04/

### Install ODBC Drivers for MSSQL Server ###

 - FreeTDS (how to install)
 - Microsoft ODBC Driver 11 for SQL Server on Linux (how to install)

#### FreeTDS (how to install) ####

`sudo apt-get install freetds-dev freetds-bin tdsodbc`

Configure

Point odbcinst.ini to the driver in /etc/odbcinst.ini:

```
[FreeTDS]
Description = v0.91 with protocol v7.2
Driver = /usr/lib/x86_64-linux-gnu/odbc/libtdsodbc.so
```

Create your DSNs in odbc.ini:

```
[dbserverdsn]
Driver = FreeTDS
Server = dbserver.domain.com
Port = 1433
TDS_Version = 7.2
```

### Microsoft ODBC Driver 11 for SQL Server on Linux (how to install) ###

https://computingforgeeks.com/how-to-install-ms-sql-on-ubuntu/