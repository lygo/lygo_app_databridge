#####
# SETUP ENVIRONMENT
####
FROM ubuntu:latest
LABEL maintainer="Gian Angelo Geminiani <angelo.geminiani@gmail.com>"

RUN apt-get update -qq

#####
# SETUP APP
#####
FROM golang:latest

# Go installation
# ENV GOROOT /usr/local/go
# ENV GOPATH /home/go
# ENV PATH $PATH:/usr/local/go/bin
RUN wget https://dl.google.com/go/go1.16.linux-amd64.tar.gz
RUN tar -C /usr/local -xzf go1.16.linux-amd64.tar.gz

# Copy source files in container
WORKDIR /home/go/src/bitbucket.org/angelogeminiani/databridge
COPY . .

# Build (when container start)
CMD ["go", "build", "-o", "/app/databridge","./cmd"]

