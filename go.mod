module bitbucket.org/lygo/lygo_app_databridge

go 1.16

require (
	bitbucket.org/lygo/lygo_commons v0.1.85 // indirect
	bitbucket.org/lygo/lygo_events v0.1.8 // indirect
	bitbucket.org/lygo/lygo_ext_auth0 v0.1.18 // indirect
	bitbucket.org/lygo/lygo_ext_dbal v0.1.25 // indirect
	bitbucket.org/lygo/lygo_ext_http v0.1.19 // indirect
	bitbucket.org/lygo/lygo_ext_logs v0.1.6 // indirect
	github.com/alexbrainman/odbc v0.0.0-20200426075526-f0492dfa1575 // indirect
	github.com/andybalholm/brotli v1.0.2 // indirect
	github.com/cbroglie/mustache v1.2.0 // indirect
	github.com/gofiber/fiber/v2 v2.8.0 // indirect
	github.com/gofiber/websocket v0.5.1 // indirect
	github.com/klauspost/compress v1.12.2 // indirect
	github.com/valyala/fasthttp v1.24.0 // indirect
	golang.org/x/sys v0.0.0-20210426230700-d19ff857e887 // indirect
)
